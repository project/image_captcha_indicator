CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

 * This module can help improve user experience when input captcha code.

 * Lightweight: js file is added in footer.

 * Saving: when input code length is bigger or equal to half of captcha code
   length then the js function start to work.


REQUIREMENTS
------------

This module requires the following modules:

 * CAPTCHA (https://drupal.org/project/captcha)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * The configuration page is at admin/config/people/captcha,
   where you can configure the CAPTCHA module
   and enable the image CAPTCHA for the desired forms.


MAINTAINERS
-----------

Current maintainers:
 * qqboy - https://www.drupal.org/u/qqboy

This project has been sponsored by:
 * Capgemini
