(function ($) {
    Drupal.behaviors.imageCaptchaIndicator = {
        attach: function (context) {
            var solution;
            var code_length = Drupal.settings.image_captcha_indicator_code_length;
            var refresh_installed = Drupal.settings.image_captcha_indicator_refresh;
            var case_validate = Drupal.settings.image_captcha_indicator_casevalidate;

            $('.reload-captcha').click(function(){
                $(this).addClass('imagecaptchaindicator-clicked');
            });

            if($('#edit-captcha-response').length > 0){
                var template = '<div style="height:5px;overflow:hidden" class="image-capthca-indicator-wrapper">';
                for(var i = 0; i <= code_length; i++){
                    template += '<span style="float:left;height:5px;" class="image-capthca-indicator-inline image-capthca-indicator-inline-'+ i +'"></span>';
                }
                template += '</div>';
                $('#edit-captcha-response').after(template);
                var total_length = $('#edit-captcha-response').outerWidth();
                $('.image-capthca-indicator-wrapper').css('width',(total_length) + 'px');
                $('.image-capthca-indicator-inline').css('display','inline-block').css('width',(total_length/code_length) + 'px');
            }
            $('#edit-captcha-response', context).keyup(function () {
                if(case_validate == 0){
                    var captcha_code = $(this).val();
                }else{
                    var captcha_code = $(this).val().toLowerCase();
                }
                $('.image-captcha-indicator-error').remove();
                $('.image-captcha-indicator-success').remove();

                if(!solution || (refresh_installed && !$('.reload-captcha').length) || (refresh_installed && $('.reload-captcha').hasClass('imagecaptchaindicator-clicked'))){
                    var $form = $(this).parents('form');
                    // Send post query for getting new captcha data.
                    var date = new Date();
                    var url = 'captcha/indicator/' + $form.attr('id').replace('-','_') +'?' + date.getTime();
                    $.get(
                        url,
                        {},
                        function (response) {
                            if(response.status == 1) {
                                if(case_validate == 0){
                                    solution = response.data['solution'];
                                }else{
                                    solution = response.data['solution'].toLowerCase();
                                }
                            }
                            else {
                                alert(response.message);
                            }
                        },
                        'json'
                    );
                }
                $('.reload-captcha').removeClass('imagecaptchaindicator-clicked');
                if(captcha_code.length > 0){
                  for(var i in solution){
                      if(solution[i] == captcha_code[i]){
                          $('.image-capthca-indicator-inline-'+i).css('backgroundColor','green');
                      }else{
                          $('.image-capthca-indicator-inline-'+i).css('backgroundColor','red');
                      }
                  }
                }else{
                    $('.image-capthca-indicator-inline').css('backgroundColor','red');
                }
            });
        }
    };
})(jQuery);
